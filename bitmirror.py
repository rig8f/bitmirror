# BitMirror
# Copyright (C) 2017 Filippo Rigotto.
# All rights reserved.

# BSD-new License
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of BitMirror nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Filippo Rigotto BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Crontab: @reboot sleep 30; su (user) -c 'python (dir)/bitmirror.py >> (dir)/bb.log 2&>1 &'
# Dependencies: pybitbucket, git, ssh
import argparse, os, signal
from pybitbucket.bitbucket import Client
from pybitbucket.auth import BasicAuthenticator
from pybitbucket.repository import Repository

SCRIPTDIR = os.path.dirname(os.path.abspath(__file__))
CONFFILE = "{0}/bmconfig.txt".format(SCRIPTDIR)
QUIET = False

def main():
    # attach interruption signals (exit on Ctrl-C)
    for sig in [signal.SIGINT,signal.SIGHUP,signal.SIGQUIT,signal.SIGTERM]:
        signal.signal(sig,sigExit)

    # read command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-q","--quiet",help="suppress output",action="store_true")
    parser.add_argument("-f","--conf",help="set config file (path) to use")
    parser.add_argument("-d","--directory",help="set local repo directory")
    parser.add_argument("-u","--username",help="set Bitbucket username")
    parser.add_argument("-p","--password",help="set Bitbucket password")
    parser.add_argument("-m","--email",help="set Bitbucket email")
    args = parser.parse_args()

    # save quiet flag and alternate config file (path) to use
    if args.quiet:
        global QUIET
        QUIET = args.quiet

    if args.conf:
        global CONFFILE
        CONFFILE = args.conf

    # by default, copy repos to /home/<user>/repos
    DIR = "{0}/repos".format(os.path.expanduser('~'))

    USER = ''
    PASS = ''
    MAIL = ''
   
    # read values from file (if it exists)
    if os.path.exists(CONFFILE):
        print "Loading values from config file {0}".format(CONFFILE)
        cnf = open(CONFFILE,'r')
        cnfval = cnf.read().split('\n')
        cnf.close()
        for val in cnfval:
            if val.startswith('repodir='):
                DIR = val[8:]
            elif val.startswith('user='):
                USER = val[5:]
            elif val.startswith('pass='):
                PASS = val[5:]
            elif val.startswith('mail='):
                MAIL = val[5:]

    # set the associated global vars
    # manual settings via args overrides file defaults
    if args.directory:
        DIR = args.directory
    if args.username:
        USER = args.username
    if args.password:
        PASS = args.password
    if args.email:
        MAIL = args.email
    
    # exit in absence of good values (file or args)
    if USER == '' or PASS == '' or MAIL == '':
        if not QUIET:
            print "ERROR during parameter parsing, exiting."
        exit(1)

    #-----

    # create dir if it doesn't exist
    if not os.path.exists(DIR):
        os.mkdir(DIR)

    # search local repos
    if not QUIET:
        print "Getting local repo list (directory {0})...".format(DIR)
    local = listRepoLocal(DIR)
    if not QUIET:
        print "Found {0} repo(s)\n".format(len(local))

    # set online account
    if not QUIET:
        print "Logging into Bitbucket..."
    BB = Client(BasicAuthenticator(USER,PASS,MAIL))
    if not QUIET:
        print "Logged in as {0}\n".format(USER)

    # search online repos
    online = None
    if not QUIET:
        print "Getting online repo list for user {0}...".format(USER)
    try:
        online = listRepoOnline(BB)
        if not QUIET:
            print "Found {0} repo(s)".format(len(online))
    except:
        if not QUIET:
            print "ERROR connecting to the account, exiting"
        exit(1)

    # mirror or update repos
    # mirror implies update
    try:
        if len(online) > 0:
            for x in online:
                if x not in local:
                    if not QUIET:
                        print "\n*** Mirroring {0}...".format(x)
                    mirror(DIR,x,USER)
                else:
                    if not QUIET:
                        print "\n*** Updating {0}...".format(x)
                    update(DIR,x)
        else:
            if not QUIET:
                print "No repo to mirror or update"
    except:
        if not QUIET:
            print "ERROR during last operation"

def listRepoLocal(dir):
    repos = os.listdir(dir)
    # remove '.git' from the end of the name
    return [x[:-4] for x in repos if '.git' in x]

def listRepoOnline(bbuser):
    repos = Repository.find_repositories_by_owner_and_role(client=bbuser, role='owner') 
    # extract repo names from the objects
    return [r.name.lower() for r in repos]

def mirror(dir,name,user):
    os.system("cd {0} && git clone {1} --mirror git@bitbucket.org:{2}/{3}.git".format(dir,'-q' if QUIET else '',user,name))
    
def update(dir,name):
    os.system("cd {0}/{1}.git && git fetch origin {2}".format(dir,name,'-q' if QUIET else ''))
    
def sigExit(sig,frame):
    if not QUIET:
        print "Interruption signal received, exiting."
    exit(1)

if __name__ == "__main__":
    main()
