# BitMirror #

A Python script that handles local Bitbucket **git** repo mirrors using its official API:  
mirror and update online repositories to a local folder.

### Dependencies / Requisites ###
* An account at [Bitbucket](https://bitbucket.org), of course :)
* [Python-bitbucket](https://bitbucket.org/atlassian/python-bitbucket)  
  Install using `sudo pip install pybitbucket`
* [Git](https://git-scm.com)  
  Install (in Debian/Ubuntu) using `sudo apt-get install git`

### Setup ###

* Generate an SSH key pair and upload public part to Bitbucket account settings;  
*required to avoid password prompts during operations* (more info 
[here](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html), follow points 2 and 4)
* Download dependencies and script, in a terminal `cd` to the directory where it's saved
* Make the script executable with `chmod +x bitmirror.py`  
1. Use `bmconfig-template.txt` to set output directory and Bitbucket account credentials:  
Copy the template with `cp bmconfig-template.txt bmconfig.txt`  
and edit `bmconfig.txt` *(the only file the script reads from)*  
2. or set it using program's parameters (see `python bitmirror.py --help`)
* Run the script with `python bitmirror.py`
* You can also set it to autostart at boot or scheduled intervals via crontab.

### AutoStart at boot ###

* Type `crontab -e` in a terminal
* add at the end, changing *(dir)* with the absolute path of the script's directory,
```
@reboot sleep 30; python (dir)/bitmirror.py >> (dir)/bb.log 2&>1 &
```
* Save and exit
* Reboot the system

----------

Uno script Python che gestisce mirror locali di repository **git** in Bitbucket usando le sue API ufficiali:  
copia e aggiorna repo online in una cartella locale.

### Dipendenze / Requisiti ###
* Un account di [Bitbucket](https://bitbucket.org), naturalmente :)
* [Python-bitbucket](https://bitbucket.org/atlassian/python-bitbucket)  
  Si installa con `sudo pip install pybitbucket`
* [Git](https://git-scm.com)  
  Si installa (in Debian/Ubuntu) con `sudo apt-get install git`

### Setup ###

* Generare una coppia di chiavi SSH e caricarne la parte pubblica nelle impostazioni dell'account Bitbucket;  
*necessario per evitare richieste di password durante le operazioni* (più info 
[qui](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html), seguire i punti 2 e 4)
* Scaricare le dipendenze e lo script, in un terminale cambiate cartela con il comando `cd` verso dove lo script risiede
* Rendere lo script eseguibile con `chmod +x bitmirror.py`  
1. Usare `bmconfig-template.txt` per impostare la cartella delle operazioni e le credenziali dell'account Bitbucket:  
Copiare il template con `cp bmconfig-template.txt bmconfig.txt`  
e modificare `bmconfig.txt` *(l'unico file che lo script legge)*  
2. o impostare il tutto con i parametri del programma (vedere `python bitmirror.py --help`)
* Eseguire lo script con `python bitmirror.py`
* Si può anche impostare perchè parta all'avvio del sistema o a intervalli regolari tramite crontab.

### AutoStart all'avvio ###

* Digitare `crontab -e` in un terminale
* aggiungere alla fine, cambiando *(dir)* con il percorso assoluto della cartella dello script,
```
@reboot sleep 30; python (dir)/bitmirror.py >> (dir)/bb.log 2&>1 &
```
* Salvare e uscire
* Riavviare il sistema

----------

### BSD-new License ###

Copyright (C) 2017, Filippo Rigotto.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,  
  this list of conditions and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice,  
  this list of conditions and the following disclaimer in the documentation and/or  
  other materials provided with the distribution.  
* Neither the name of BitMirror nor the names of its contributors may be used  
  to endorse or promote products derived from this software without specific prior written permission.  

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED  
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  
DISCLAIMED. IN NO EVENT SHALL Filippo Rigotto BE LIABLE FOR ANY  
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES  
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;  
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND  
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT  
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS  
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
